# creat2id

Ce plugin a pour objet de permettre la création d'un visiteur avec une double authentification (vérification en deux étapes, une méthode d'authentification forte).

## 1. Présentation des objectifs de l'outil :

**Objet :** Ce plugin a pour objet de permettre la création d'un visiteur avec une double authentification.
"La double authentification ou vérification en deux étapes est une méthode d'authentification forte par laquelle un utilisateur peut accéder à une ressource informatique après avoir présenté deux preuves d'identité distinctes à un mécanisme d'authentification." définition : Wikipédia

**Méthode :** La création standard d'un auteur avec SPIP se fait sur la base d'une identification avec son mail. C'est une première clé d'identification. Le plugin propose d'ajouter une seconde clé, qui sera un indentifiant unique dont le logiciel vérifiera la conformité.

**Avertissement :** Le cryptage retenu et la vérification faite de la seconde preuve d'identité n'est pas du ressort du plugin. Le développeur pourra développer ce qu'il souhaite, du scan de l'oeil jusqu'à un simple md5 sur le mail de l'utilisateur. Une option minimaliste est fournie par défaut en modèle pédagogique.

**Limitation :** la double authentification n'a pas vocation a être à chaque connexion mais bien à la création du compte seulement.


## 2. Documentations utiles :

La balise #FORMULAIRE_INSCRIPTION{…} affiche le formulaire permettant à vos visiteurs de s’inscrire automatiquement, c’est à dire sans aucune intervention du ou des responsables du site. C'est elle que nous allons modifier.
https://www.spip.net/fr_article4590.html

Les principes généraux pour modifier un squelette proposé par défaut par SPIP. Il faudra peut être modifier les squelettes du site pour faire appraitre la balise #FORMULAIRE_INSCRIPTION avec les paramètres souhaités. (voir ci-dessous 3. a)
https://www.spip.net/fr_article3437.html

Comment surcharger une fonction ? Il sera nécessaire de le faire pour avoir une clé d'authentification personnalisée à vos besoins `../creat2id/creat2id/cle_activation.php` et le traitement adhoc `../creat2id/creat2id/id_reussie.php`.
https://programmer.spip.net/Surcharger-une-fonction-_dist

Dans le prolongement du précédent article, celui-ci décrit l'usage de la fonction ’charger_fonction’
https://programmer.spip.net/charger_fonction

La balise #SESSION_SET permet d’insérer dans la balise #SESSION des données supplémentaires
https://www.spip.net/fr_article3984.html

La balise #SESSION, permet d’accéder aux informations liées au visiteur authentifié et de différencier automatiquement le cache en fonction du visiteur.
https://www.spip.net/fr_article3979.html


## 3. Présentation des principes de programmation de l'outil :

Une prise en main facile se fait, une fois que vous avez indiquer dans vos préférences que vous souhaitiez voir le menu développement, en selectionnant Généré une clé depuis ce menu.

### a) le processus d'inscription d'un nouveau visiteur

Le squelette squelettes-dist/sommaire.html de la dist appele la balise #FORMULAIRE_INSCRIPTION. Pour que cette balise ait un effet, il faut avoir configurer SPIP depuis l’espace privé, depuis le menu Configuration → Interactivité et indiquer "Accepter l’inscription de visiteurs du site public" dans le bloc « Visiteurs ».
Si l'on ne veut que l'inscription de visiteurs, et seulement eux, la balise peut prendre la forme suivante :
	[(#REM) Si vous êtes connecté, la balise n’affiche rien.) 
	[(#FORMULAIRE_INSCRIPTION{6forum})]
Cette balise ainsi paramétrée peut être placée dans la personnalisation que vous ferez du script squelettes/sommaire.html. 
Une fois le formulaire rempli (nom ou pseudo + email) et validé, trois évènement se succèdent pour peu que votre site puisse envoyer des emails: 1) un message avertit le visiteur : "Votre nouvel identifiant vient de vous être envoyé par email." ; 2) un email est envoyé à l’adresse indiquée ; 3) un nouveau compte est créé dans SPIP avec le statut "Inscription à confirmer".	

On le voit, la clé d'identification de l'inscription définitive de l'auteur, pour SPIP, est le mail. L'objet du présent plugin est d'y ajouter une autre clé.

### b) les étapes de la programmation

La balise #FORMULAIRE_INSCRIPTION{…}  renvoie à un formulaire dont les fichiers constitutifs sont au nombre de trois :
	`squelettes-dist/formulaires/inscription.html & php`
	`squelettes-dist/formulaires/inc-inscription-explication.html`
	
Ce jeu de squelettes proposé par défaut par SPIP dans le répertoire `squelettes-dist/` peut être modifié. Vous voulez installer un autre jeu de squelettes, en créant un répertoire nommé `squelettes/` au même niveau dans lequels vous placerez les squelettes qui se substitueront au jeu par défaut. 

Le présent plugin ne procède pas de la sorte. Il utilise les pipelines (formulaire_charger, formulaire_verifier, formulaire_traiter) pour s'inscrire dans le  flux de données du formulaire d'inscription existant et y ajouter des éléments pour les traitements PHP relatif à la clé d'authentification. Pour modifier le formulaire dans sa partie HTML, le plugin procède également en utilisant un pipeline (`formulaire_fond`) pour modifier l'affichage.

### c) la seconde clé d'authentification

Il s'agit d'un code que vous avez transmis à votre utilisateur (dans un courrier, sur une carte d'adhésion...) et qu'il va renseigner dans le formulaire de création de son compte en ligne. Le formulaire va analyser le code de l'utilisateur (seconde clé) en regardant s'il correspond à son mail (première clé). Si la seconde clé n'est pas valide, il ne pourra pas créer son compte.

La seconde clé d'authentification est :
	créée
		par la fonction `creat2id_cle_authentification_dist ($arg1)`
	vérifiée
		par la fonction `creat2id_cle_activation_dist ($cle_donnee, $mail, $nom)`
Ces fonctions peuvent être surchargées en créant dans un plugin un répertoire creat2id et en y plaçant deux fichiers, `cle_authentification.php` et `cle_activation.php` contenant respectivement les deux fonctions dénommées `creat2id_cle_authentification ($arg1)` et `creat2id_cle_activation ($cle_donnee, $mail, $nom)`. La fonction `creat2id_cle_activation ($cle_donnee, $mail, $nom)` est appelées dans la partie `verifier() du formulaire d'inscription`.

L'utilisateur peut souhaiter faire un traitement particulier à l'issue de l'identification réussie. Pour ce faire une fonction accueil ce traitement: `creat2id_id_reussie ($cle, $desc)`. Cette fonction est appelée dans la partie `traiter()` du formulaire d'inscription.
