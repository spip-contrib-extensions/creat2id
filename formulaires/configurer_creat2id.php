<?php
/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_creat2id_saisies_dist(){
	// tableau décrivant les saisies à afficher dans le formulaire de configuration
	return = [
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'remplacement',
				'label' => '<:creat2id:configurer_messages:>',
				'explication' => '<:creat2id:configurer_messages_explication:>',
				'label_case' => '<:creat2id:configurer_messages_label_case:>',
			]
		]
	];
}