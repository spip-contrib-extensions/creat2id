<?php
/**
 * Administration de l'installation et de la désintallation du plugin creat2id
 *
 * @plugin     creat2id
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Creat2id\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
  * Installation/maj des tables creat2id
  *
  * @param string $nom_meta_base_version
  * @param string $version_cible
  */

function creat2id_upgrade($nom_meta_base_version, $version_cible){

	$maj = [];
	$maj['create'] = [
		//array('maj_tables', array('spip_table', 'spip_table_liens')),
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
  * Desinstallation/suppression des tables creat2id
  *
  * @param string $nom_meta_base_version
  */

function creat2id_vider_tables($nom_meta_base_version) {
	//sql_drop_table("spip_table");
	//sql_drop_table("spip_table_liens");
	effacer_meta($nom_meta_base_version);
	// Effacer la config
 	effacer_meta('creat2id');
}

