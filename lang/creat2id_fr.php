<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'configurer_messages_explication' => 'Cocher cette option permet de remplacer le bloc &lt;legend&gt; et ses idiomes par une noisette à créer et à placer dans le répertoire <i>formulaires/</i> et dénommée <i>inc-inscription-cle.html</i>. En cas d’absence, le plugin substituera une valeur par défaut.<br />Pensez également à la noisette existante <i>formulaires/inc-inscription-explication.html</i> qui peut être adaptée ou son contenu tout simplement supprimé.',
	'configurer_messages_label_case' => 'Remplacement',
	'configurer_messages' => 'Adapter les messages lors de l’inscription',
	'configurer' => 'Configurer Creat2id',
	'erreur_cle_necessaire' => 'La clé est nécessaire',
	'erreur_cle' => 'Erreur dans votre clé',
	'inscription_entree_cle' => 'Numéro unique',
	'inscription_entree_mail' => 'Mail',
	'inscription_entree_nom' => 'Nom ou prénom',
	'texte_modus_operandi' => 'Cette clé, introduite avec le mail, permettra à la personne qui visite votre site de s’inscrire en tant qu’<code>auteur</code> au statut de <code>visiteur</code>.',
	'texte_process' => 'Cette clé, introduite dans le formulaire d’identification, génère comme résultat :',
	'titre_creat2id' => 'Creat2id',
	'titre_generer_cle' => 'Générer une clé',
	'titre_inscription_explication' => 'Munissez vous de votre carte d’adhérent·e pour indiquer votre numéro unique dans le formulaire ci-dessous.<br>Cette démarche faite, votre mot de passe vous parviendra rapidement, par courrier électronique.',
	'titre_inscription' => 'Créer un compte d’adhérent·e',
	'titre_test' => 'Test de la clé et du process',
	'titre_tester_cle' => 'Tester la clé',
];
