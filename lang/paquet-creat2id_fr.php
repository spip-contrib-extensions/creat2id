<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'creat2id_description' => 'Ce plugin a pour but de permettre la création d’un auteur avec une double authentification.',
	'creat2id_nom' => 'Creat2ID',
	'creat2id_slogan' => 'Une double authentification à la création du compte'
];
