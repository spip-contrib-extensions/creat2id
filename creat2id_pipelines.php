<?php
/**
 * Utilisations de pipelines par creat2id
 *
 * @plugin     creat2id
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Creat2id\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion dans le pipeline formulaire_fond (SPIP)
 *
 * @param array $flux
 *        Le contexte du pipeline
 * @return array $flux
 *        Le contexte du pipeline modifié
 */
function creat2id_formulaire_fond($flux) {

	if ($flux['args']['form'] == 'inscription'){
		include_spip('inc/config');
		$search = []; $replace = [];

		if ($flux['args']['contexte']['_mode'] == '6forum' && $flux['args']['contexte']['editable']) {

			# retirer l'explication relative à l'identification dans un forum, si demandé
			if (lire_config("creat2id/remplacement", 0)) {
				if (($p = strpos($flux['data'], "<legend>")) !== false) {
					$search[] = '<legend>' . _T('form_forum_identifiants') . '</legend>';
					$replace[] = "";
					$search[] = "<p class='explication'>" . _T('form_forum_indiquer_nom_email') . "</p>";
					$replace[] = "";
				}
			}
			# prévoir de placer la clé (avant le mail)...  
			$search[] = "<div class='editer saisie_mail_inscription obligatoire";
			# ... via une noisette si le webmestre la définie ...
			if (find_in_path('formulaires/inc-inscription-cle.html')) {
				$replace[] = recuperer_fond('formulaires/inc-inscription-cle', $flux['args']['contexte']) . "<div class='editer saisie_mail_inscription obligatoire";
			} else {
			# ... ou une variable, en l'absence
				$replace[] = "<div class='editer saisie_cle_inscription obligatoire'><label for='cle_inscription'>" . _T('creat2id:inscription_entree_cle') . "<em class='obligatoire'>(obligatoire)</em></label><input type='text' class='text' name='cle_inscription' id='cle_inscription' value='' size='30'  required='required' autocapitalize='off' autocorrect='off' /></div>" . "<div class='editer saisie_mail_inscription obligatoire";
			}
			# faire la substitution
			$ajout = str_replace($search, $replace, $flux['data']);
			if ($ajout) {
				$flux['data'] = $ajout;
			}
		}
	}
	return $flux;
}



/**
 * Insertion dans le pipeline formulaire_charger (SPIP)
 *
 * @param  array $flux
 *         Le contexte du pipeline
 * @exemple pour inscription
 *         [args] [form] nom du formulaire
 *               [args] argument l'accompagnant
 *               [je_suis_poste] comme son nom l'indique
 *         [data] [nom_inscription] la demande du nom
 *               [mail_inscription] et du mail
 *               [id] 
 *               [_mode] => le mode (6forum...)
 * @return array $flux
 *         Le contexte du pipeline modifié
 */
function creat2id_formulaire_charger($flux) {
	# accéder au formulaire d'inscription, pour insérer la demande de la clé
	if (is_array($flux)
		and isset($flux['args']['form'])
		and $flux['args']['form'] == 'inscription' 
		and $flux['data']['_mode'] == '6forum' 
	){

		# définir l'information
		$valeurs = array();
		$valeurs['cle_inscription'] = '';

		# ajouter l'information dans le flux
		if (is_array($flux['data'])) {
			$flux['data'] = array_merge($flux['data'], $valeurs);
		} else {
			$flux['data'] = $valeurs;
		}
		
	}

	return $flux;
}

/**
 * Insertion dans le pipeline formulaire_verifier (SPIP)
 *
 * @param array $flux
 *        Le contexte du pipeline
 * @return array $flux
 *        Le contexte du pipeline modifié
 */
function creat2id_formulaire_verifier($flux) {

	# accéder au formulaire d'inscription, pour vérifier la clé
	if (is_array($flux)
		and isset($flux['args']['form'])
		and $flux['args']['form'] == 'inscription') 
	{
		if ($cle_donnee = strval(_request('cle_inscription'))) 
		{
			# récupérer le nom et le mail
			$nom = _request('nom_inscription');
			$mail = _request('mail_inscription');
			if ($cle_activation = charger_fonction('cle_activation','creat2id')){
				# activer la clé sur le mail et le nom
				if (!$cle_activation = $cle_activation($cle_donnee, $mail, $nom)) {
					$flux['data']['message_erreur'] = _T('creat2id:erreur_cle');
spip_log($flux, 'creat_id.' . _LOG_ERREUR);
	
				}
			}
		}
	}
	return $flux;

}

/**
 * Insertion dans le pipeline formulaire_traiter (SPIP)
 *
 * @param array $flux
 *        Le contexte du pipeline
 * @return array $flux
 *        Le contexte du pipeline modifié
 */
function creat2id_formulaire_traiter($flux) {

	if (is_array($flux)
		and isset($flux['args']['form'])
		and $flux['args']['form'] == "inscription"
		# récupérer la clé
		and $cle = (string) _request('cle_inscription')
		# récupérer l'id_auteur
		and isset($flux['data']['id_auteur'])
		and $id_auteur = (int) $flux['data']['id_auteur']
		# on s'assure que c'est bien un visiteur
		and isset($flux['args']['args'][0])
		and $flux['args']['args'][0] == '6forum'
		# et que le traitement est ok
		and isset($flux['data']['message_ok'])
	) {
		# SPIP a déjà créé l'auteur mais on a besoin des informations
		if ($desc = sql_fetsel('*', 'spip_auteurs', 'id_auteur=' . $id_auteur)){
			# appeler la fonction de traitement suite à la validation de la création du compte
			$id_reussie = charger_fonction('id_reussie', 'creat2id');
			$traitement = $id_reussie($cle, $desc);
		}
	}
return $flux;
}
