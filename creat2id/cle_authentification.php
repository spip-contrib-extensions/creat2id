<?php
/**
 * Fonction du plugin Creat2id pour créer une clé
 *
 *
 * @plugin     Creat2id
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Creat2id\Creat2id
 */
 

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
	}
	
/**
 * La fonction creat2id_cle_authentification retourne la clé d'identification liée à l'argument.
 *
 * Cette fonction, qui n'a qu'un visée pédagogique, postule
 * que vous souhaitez faire une clé à partir du mail du prospect
 *
 * @exemple :
 * la clé sera pour
 * jean.valjean@victor-hugo.fr
 * sous la forme :
 * 1e27c37e
 * Vous communiquerez donc vers Jean VALJEAN en lui disant des s'identifier avec son mail et sa clé.
 */

function creat2id_cle_authentification_dist (string $email) {
	return substr(md5($email),0,8);
}
