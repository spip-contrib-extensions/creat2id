<?php
/**
 * fonction activée lors de la réussite de l'authentification
 *
 * @plugin     creat2id
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Creat2id\Creat2id
 */
 

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
	}
	
/**
 * Réussir n'est que le début.
 *
 * @param string $cle_transmise
 *        La clé d'identification
 * @param array $desc
 *        Les valeurs de l'auteur
 * @return void
 */

function creat2id_id_reussie_dist (string $cle, array $desc) {
	// la fonction se contente de dire dans le log que la création a réussie 
	// mais une surcharge pourrait lui faire réaliser d'autres opérations
	spip_log('Identification réussie de l’auteur n°' . $desc['id_auteur'] . ' avec pour email ' . $desc['email'] . ' et pour clé ' . $cle, 'creat_id.' . _LOG_INFO_IMPORTANTE);
}