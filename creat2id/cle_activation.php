<?php
/**
 * vérification de la double authentification
 *
 * ```
 * if ($mafonction = charger_fonction('cle_activation','creat2id')){
 *   if ($mafonction($cle, $mail, $nom)){
 *      $retours = 'La clé est authentique et l’utilisateur l’utilise à bon escient !';
 *   } else {
 *   $retours = "La clé ($cle) ou l’utilisateur ($nom / $mail) présentent un problème !";
 *   }
 *   $data = '<em>' . $retours .'</em>';
 * } else {
 *   $data = 'La fonction n’a pas pu être chargée !!';
 * }
 * ```
 *
 * @plugin     creat2id
 * @copyright  2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Creat2id\Creat2id
 */
 

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
	
/**
 * La clé est-elle valide ?
 *
 * @param string $cle_transmise
 *        La clé d'identification
 * @param string $mail
 *        La mail de l'auteur
 * @param string $nom
 *        Le nom ou alias de l'auteur
 * @return bolean
 *        Vrai ou Faux selon la réussite de la vérification de la clé
 */

function creat2id_cle_activation_dist (string $cle_transmise, string $mail, string $nom) {

	if ($cle_transmise = trim(strtolower($cle_transmise))){
		// vérification que mail permet de former la clé transmise
		if ($cle_authentification = charger_fonction('cle_authentification','creat2id')
			and $cle = $cle_authentification($mail)
			and $cle === $cle_transmise)
		{
			return true;
		}
	}
	return false;
}
